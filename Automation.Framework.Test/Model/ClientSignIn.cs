﻿using Automation.Framework.Base;
using Automation.Framework.Test.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unity;

namespace Automation.Framework.Test.Model
{

    public class ClientSignIn
    {
        private HomePage homePage = UnityContainerFactory.GetContainer().Resolve<HomePage>();

        public void AccessLoginPage()
        {
            homePage.Helper.BrowserHelper.Navigate(homePage.ClientPortalUrl);
            //homePage.clickSignInLink();
            string url = homePage.Helper.BrowserHelper.GetBrowserUrl();
            Assert.AreEqual(url, "https://www.tapmad.com/");
                         
        }
    }
}
