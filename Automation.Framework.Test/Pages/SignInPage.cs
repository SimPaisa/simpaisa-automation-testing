﻿using Automation.Framework.Base;
using Automation.Framework.Core;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;

namespace Automation.Framework.Test.Pages
{
    internal class SignInPage : BasePage
    {
        public SignInPage(IWebDriver driver, IConfigurationRoot config) : base(driver, config)
        {
        }
    }
}
