﻿using Automation.Framework.Base;
using Automation.Framework.Core;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;

namespace Automation.Framework.Test.Pages
{
    internal class HomePage : BasePage
    {
        public string ClientPortalUrl;
        public HomePage(IWebDriver driver, IConfigurationRoot config) : base(driver, config)
        {
            ClientPortalUrl = BasePage.AppUrl;
        }

        public IWebElement singInButton => WaitTillElementDisplayed("Sign 21", ElementLocator.ID);

       public void clickSignInLink(){
            singInButton.Click();
            // id="Sign 21"
        }

    }
}
