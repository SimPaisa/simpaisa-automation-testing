﻿using System.IO;
using Automation.Framework.Base;
using Automation.Framework.Core;
using Automation.Framework.Test.Pages;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using Unity;
using Unity.Lifetime;

namespace Automation.Framework.Test
{
    [TestClass]
    public class Hooks
    {
        [AssemblyInitialize]
        public static void BeforeTestRun(TestContext context)
        {

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(path: "appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            Driver.StartBrowser(BrowserTypes.Chrome,30, GetChromeOptions());
            UnityContainerFactory.GetContainer().RegisterInstance<IConfigurationRoot>(configuration);
            UnityContainerFactory.GetContainer().RegisterInstance<IWebDriver>(Driver.Browser);
        }

        private static InternetExplorerOptions GetIEOptions()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.IgnoreZoomLevel = true;
            options.EnableNativeEvents = true;
            options.EnsureCleanSession = true;
            options.ElementScrollBehavior = InternetExplorerElementScrollBehavior.Bottom;
            return options;
        }

        private static ChromeOptions GetChromeOptions()
        {
            ChromeOptions chromeOptions = new ChromeOptions();
            //chromeOptions.AddArgument("window-size=1920,1080");
            chromeOptions.AddArgument("window-size=1200,980");
            return chromeOptions;
        }

        [TestInitialize]
        public void BeforeTest()
        {
            // test

        }

        [TestCleanup]
        public void AfetrTest()
        {
            // Runs after each test. (Optional)
        }

        [AssemblyCleanup]
        public static void AfterTestRun()
        {
           // Driver.StopBrowser();
        }
    }
}