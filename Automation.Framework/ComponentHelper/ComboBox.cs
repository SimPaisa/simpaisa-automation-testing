﻿using Automation.Framework.ComponentHelper.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.Framework.ComponentHelper
{
    /// <summary>
    /// 
    /// </summary>
    public class ComboBox : IComboBoxHelper
    {
        private static SelectElement select;

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public bool IsComboBoxEnabled(IWebElement element)
        {
            return element.Enabled;
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="index"></param>
        public void SelectElementByIndex(IWebElement element, int index)
        {
            select = new SelectElement(element);
            select.SelectByIndex(index);
        }

       
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="value"></param>
        public void SelectElementByValue(IWebElement element, string value)
        {
            select = new SelectElement(element);
            select.SelectByValue(value);
        }

      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="visibleText"></param>
        public void SelectElementByVIsibleText(IWebElement element, string visibleText)
        {
            select = new SelectElement(element);
            select.SelectByText(visibleText);
        }
    }
}